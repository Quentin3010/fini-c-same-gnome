#include "main.h"

#include "game.h"
#include "generations_plateau.h"
#include "saisie.h"
#include "update_plateau.h"
#include "gravite.h"

int main () {
	system("clear"); //vide le terminal

	Game game; /*Structure game correspondant à la partie en cours*/
	create_game(&game);
	
	char path[] = "./Final/V3/init3.txt"; /*chemin d'accès vers le plateau .txt*/
	generate_plateau_from_txt(&game, path);
	//generate_plateau_random(&game);
	
	int x,y; /*coordonnés de la ligne et de la colonne que le joueur à choisis*/
	
	//Si le plateau est vide (case [0][0] vide), alors le jeu est fini
	while(verif_non_vide(game)!=0){
		//Le joueur choisit une case avec une bille
		x = -1; y = -1;
		saisie(game, &x, &y);
		
		//Le programme affiche toutes les billes voisines (change en "x")
		system("clear");
		affichage_groupe(&game, x, y);
		affichage_plateau(game);
		sleep(1);
		
		//On fait tomber les billes vers le bas (et on calcul le score)
		system("clear");
		gravite(&game);
		affichage_plateau(game);
		sleep(1);
		
		//On les regroupes vers la gauche
		system("clear");
		gravite_gauche(&game);
		
		//Et on passe au round suivant
		game.nbTour++;
	}
	
	system("clear");
	affichage_plateau(game);
	
	//Ecran de fin (score + nombre de tour) 
	sleep(2);
	system("clear");
	printf("*************************************************\n*                                               *\n*                                               *\n*                FELICITATION !!!               *\n*                                               *\n*                                               *\n*                 score : %5d                 *\n*                                               *\n*                  tour : %3d                   *\n*                                               *\n*                                               *\n*                                               *\n*************************************************\n", game.score, game.nbTour);
	return 0;
}
