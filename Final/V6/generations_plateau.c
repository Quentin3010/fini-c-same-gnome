#include "main.h"
#include "generations_plateau.h"

/**************************************************/
/* Fonction generate_plateau_from_txt             */
/* Role : Génère un plateau depuis un fichier TXT */
/*    Description des paramètres                  */
/*    game : Structurte Game - Résultat           */
/*    path : chaine de caractère - Donnée         */
/**************************************************/
/* Declaration et traitement */
void generate_plateau_from_txt(Game* game, char path[]){
	FILE* ptr = fopen(path, "r"); /*fichier txt chargé depuis le "path" dans lequel se trouve le plateau*/
	char ch; /*variable temporaire dans laquelle on stocke chaque caractère lu un par un*/
	int i = 0; /* indice de boucles */
	
	if (NULL == ptr) {
        	printf("Le plateau n'a pas pu être chargé depuis \"%s\"\n",path);
    	}
    	
    	do{
        	ch = fgetc(ptr);
        	if(ch!='\n'){ //on ignore les retours à la ligne comme étant des caractères pour le plateau
 			(*game).plateau[i/COL][i%COL] = ch;
 			i++;
 		}
    	}while (ch != EOF); //EOF = end of file (fin du fichier)
    	fclose(ptr);
}
