#include "main.h"
#include "saisie.h"
#include "game.h"

/********************************************************/
/* Action saisie                                        */
/* Role : Permet au joueur de saisir ses choix du jeu   */
/*    Description des paramètres                        */
/*    game : Structure Game - Donnée                    */
/*    y : Entier - Colonne saisie par le joueur -  R    */
/*    x : Entier - Ligne saisie par le joueur -    R    */
/********************************************************/
/* Declaration et traitement */
void saisie(Game game, int* x, int* y){	
	char saisie_utilisateur[6];
	do{
		system("clear");
		affichage_plateau(game);
		printf("N° de colonne et ligne ? \n");
		fgets(saisie_utilisateur, 6, stdin);
    		sscanf(saisie_utilisateur, "%d %d", y, x);
	}while(*x<0 || *x>=LIG || *y<0 || *y>=COL || game.plateau[*x][*y]=='.');
	/* il faut que l'emplacement saisi contienne une bille, càd contienne un caractère différent du point */
	/* et qu'il corresponde bien à la taille du plateau, càd N° de colonne comprise entre 0 et 14 et N° de ligne entre 0 et 9 */
}
