#include "main.h"
#include "game.h"
#include "saisie.h"
#include "update_plateau.h"
#include "gravite.h"

#include <math.h> /* A inclure dans le fichier SG_math.c */

/********************************/
/* Fonction f1                  */
/* Source : Fichier initmath.c  */
/********************************/
/* Declaration et traitement */
double f1 (int i, int j){
	return sin((double)i*j + (double)j);
}


/********************************/
/* Fonction f2                  */
/* Source : Fichier initmath.c  */
/********************************/
/* Declaration et traitement */
double f2 (int i, int j){
	double z = f1 (i, j);
	return z * z;
}


/********************************************************/
/* Action generate_plateau_math                         */
/* Role : Permet le remplissage du plateau              */
/*    Description des paramètres                        */
/*    game : Structure Game - Résultat                  */
/********************************************************/
/* Declaration et traitement */
void generate_plateau_math(Game* game){
	
	int i, j; /* indices de boucles */
	float z; /* variable qui récupère la fonction f2 */
	
	for(i=0; i<LIG; i++){
		for(j=0; j<COL; j++){
		
			z = f2(i,j);
			
			/* la bille est rouge */
			if(z<0){
				(*game).plateau[i][j] = 'R';
			}
			/* la bille est verte */
			else if(z<0.5){
				(*game).plateau[i][j] = 'V';
			}
			/* la bille est bleue */
			else{
				(*game).plateau[i][j] = 'B';
			}
		}
	}
}	



int main () {
	system("clear"); //vide le terminal

	Game game; /*Structure game correspondant à la partie en cours*/
	create_game(&game);

	generate_plateau_math(&game);
	
	int x,y; /*coordonnés de la ligne et de la colonne que le joueur à choisis*/
	
	//Si le plateau est vide (case [0][0] vide), alors le jeu est fini
	while(verif_non_vide(game)!=0){
		//Le joueur choisit une case avec une bille
		x = -1; y = -1;
		saisie(game, &x, &y);
		
		//Le programme affiche toutes les billes voisines (change en "x")
		system("clear");
		affichage_groupe(&game, x, y);
		affichage_plateau(game);
		sleep(1);
		
		//On fait tomber les billes vers le bas (et on calcule le score)
		system("clear");
		gravite(&game);
		affichage_plateau(game);
		sleep(1);
		
		//On les regroupe vers la gauche
		system("clear");
		gravite_gauche(&game);
		
		//Et on passe au round suivant
		game.nbTour++;
	}
	
	system("clear");
	affichage_plateau(game);
	
	//Ecran de fin (score + nombre de tour) 
	sleep(2);
	system("clear");
	printf("*************************************************\n*                                               *\n*                                               *\n*                FELICITATION !!!               *\n*                                               *\n*                                               *\n*                 score : %5d                 *\n*                                               *\n*                  tour : %3d                   *\n*                                               *\n*                                               *\n*                                               *\n*************************************************\n", game.score, game.nbTour);
	return 0;
}
