#include "main.h"
#include "game.h"

/***************************************/
/* Action create_game                  */
/* Role : Initialise la structure Game */
/*   Description des paramètres :      */
/*   game : Structure Game - Resultat  */
/***************************************/
/* Declaration et traitement */
void create_game(Game* game){
	(*game).score = 0;
	(*game).scoreTour = 0;
	(*game).nbTour = 0;
	(*game).nbColRestant = COL;
	reset_hasX(game);
}

/**********************************************************/
/* Action reset_hasX                                      */
/* Role : Mets à 0 le vecteur "hasX" d'une structure Game */
/*   Description des paramètres :                         */
/*   game : Structure Game - Resultat                     */
/**********************************************************/
/* Declaration et traitement */
void reset_hasX(Game* game){
	int i; /* indice de boucles */
	for(i = 0; i<COL; i++){
		(*game).hasX[i] = 0;
	} 
}

/*************************************************/
/* Fonction verif_non_vide                       */
/* Role : Vérifie que le plateau n'est pas vide  */
/*    Description des paramètres                 */
/*    game : Structurte Game - Donnée            */
/*    B : Booléen - Résultat                     */
/*************************************************/
/* Declaration et traitement */
int verif_non_vide(Game game){
	return(game.plateau[0][0]!='.'); /* c'est la case en bas à gauche du plateau, si le plateau n'est pas vide alors elle a forcément un caractère */
	
}

/*************************************************/
/* Action affichage_plateau                      */
/* Role : Affiche le plateau de jeu              */
/*    Description des paramètres                 */
/*    game : Structure Game - Donnée             */
/*************************************************/
/* Declaration et traitement */
void affichage_plateau(Game game){
	int i, j, k;  /* indices de boucles */
	
	printf("Tour n°%d / Score = %d (+%d)\n", game.nbTour, game.score, game.scoreTour);
    /* Affichage de la matrice */
	for(i=LIG-1; i>=0; i--){
		printf("%2d  |",i);
		for(j=0; j<COL; j++){
			affichage_case(game.plateau[i][j]);
		}
		printf("\n");
	}
    /* Affichage de la barre horizontale en bas du plateau de jeu */
    for(k=0; k<80; k++) printf("_");
    printf("\n");
    /* Affichage des indices de colonnes */
    printf("     ");
    for(j=0; j<COL; j++) printf("%2d   ",j);
    printf("\n");
    
}

/*************************************************/
/* Action affichage_case                         */
/* Role : Affiche une case avec la bonne couleur */
/*    Description des paramètres                 */
/*    c : caractère - Donnée                     */
/*************************************************/
/* Declaration et traitement */
void affichage_case(char c){
	if(c=='R'){
		printf ("%s%s%4c%s ", rouge, reverse, ' ', reset);
	}else if(c=='B'){
		printf ("%s%s%4c%s ", bleu, reverse, ' ', reset);
	}else if(c=='V'){
		printf ("%s%s%4c%s ", vert, reverse, ' ', reset);
	}else if(c=='x'){
		printf ("%s%s%4c%s ", jaune, reverse, ' ', reset);
	}else{
		printf ("%4c ", ' ');
	}
}
