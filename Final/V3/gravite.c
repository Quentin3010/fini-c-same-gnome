#include "main.h"
#include "gravite.h"
#include "game.h"

/*
int main(){
	char plateau[LIG][COL];
	char path[] = "./plateaux_txt/plateau_test4.txt";
	int nbColRestant = COL;
	
	generate_plateau_from_txt(plateau, path);
	affichage_plateau(plateau, 1);
	
	gravite(plateau);
	gravite_gauche(plateau, &nbColRestant);
	
	affichage_plateau(plateau, 1);
	return 0;
}
*/

/********************************************************/
/* Action gravite                                       */
/* Role : Effectue l'écroulement des billes             */
/*    Description des paramètres                        */
/*    game : Strcuture Game - Donnée/Résultat           */
/********************************************************/
/* Declaration et traitement */
void gravite(Game* game){
	
	int i, j, lig; /* indices de boucles */
	int idxLastX; /*indice de ligne du dernier trou dans la colonne*/
	int xCase; /*nombre de x trouvé par itération*/
	int totalXCase; /*nombre total de x trouvé */
	//Décalage des billes
	for(j=0; j<COL; j++){
		if((*game).hasX[j]==1){
			idxLastX = 0;
			xCase = 0;
			i = 0;
			while(i<LIG && (*game).plateau[i][j]!='.'){
				if(xCase==0 && (*game).plateau[i][j]!='x'){
					idxLastX++;
				}
				else if(xCase>=1 && (*game).plateau[i][j]!='x'){
				(*game).plateau[idxLastX][j] = (*game).plateau[i][j];
					i = idxLastX + xCase;
					idxLastX = idxLastX + 1;
				}
				else{
					xCase++;
				}
				i++;
			}
			totalXCase += xCase;
			//Transformations des dernières lignes passant à vide
			for(lig = (i - xCase); lig<LIG; lig++){
				(*game).plateau[lig][j] = '.';
			}
		}
	}
	//Calcul des points
	(*game).scoreTour = (totalXCase-1)*(totalXCase-1);
	(*game).score += (*game).scoreTour;
	//Reset du vecteur hasX
	reset_hasX(game);
}

/*********************************************************/
/* Action gravite_gauche                                 */
/* Role : Effectue le décalage des billes vers la gauche */
/*    Description des paramètres                         */
/*    game : Strcuture Game - Donnée/Résultat            */
/*********************************************************/
/* Declaration et traitement */
void gravite_gauche(Game* game){
	int i, j, lig, col; /* indices de boucles */
	int idxLastColEmpty = 0; /*indice de la dernière colonne vide trouvée*/
	int emptyColFound = 0; /*Nombre de colonne vide trouvée*/
	//Décalge des colonnes
	i = 0;
	while(i<(*game).nbColRestant){
		//Si on a pas encore trouvé de colonne vide et qu'elle l'est pas, alors on passe la colonne suivante
		if(emptyColFound==0 && (*game).plateau[0][i]!='.'){
			idxLastColEmpty++;
		//Si on a déjà trouvé au moins une colonne vide et que celle où on est actuellement ne l'est pas, alors on procède au décalage
		}else if(emptyColFound>=1 && (*game).plateau[0][i]!='.'){
			for(j = 0; j<LIG; j++){
				(*game).plateau[j][idxLastColEmpty] = (*game).plateau[j][i];
			}
			i = idxLastColEmpty + emptyColFound;
			idxLastColEmpty = idxLastColEmpty + 1;
		//Si aucun des if passe, alors c'est que la colonne est vide
		}else{
			emptyColFound++;
		}
		i++;
	}
	//Transformations des dernières colones passant à vide
	for(col = ((*game).nbColRestant - emptyColFound); col<(*game).nbColRestant; col++){
		for(lig = 0; lig<LIG; lig++){
			(*game).plateau[lig][col] = '.';
		}
	}
	//Update de nbColRestant
	(*game).nbColRestant = (*game).nbColRestant - emptyColFound;
}
