#include "main.h"
#include "update_plateau.h"

/*
int main(){
	char plateau[LIG][COL];
	char path[] = "./plateaux_txt/init8.txt";
	
	generate_plateau_from_txt(plateau, path);
	affichage_plateau(plateau);
	
	system("clear");
	
	affichage_groupe(plateau, 1, 0);
	affichage_plateau(plateau);
}
*/


/****************************************************************/
/* Action affichage_groupe                                      */
/* Role : Permet d'afficher les billes qui se touchent (bloc)   */
/*    Description des paramètres                                */
/*    game : Structure Game - Donnée/Résultat                   */
/*    idxLig : Entier - Indice de ligne - Donnée                */
/*    idxCol : Entier - Indice de colonne - Donnée              */
/****************************************************************/
/* Declaration et traitement */
void affichage_groupe(Game* game, int idxLig, int idxCol){
	char initialChar = (*game).plateau[idxLig][idxCol];
	(*game).plateau[idxLig][idxCol] = 'x';
	(*game).hasX[idxCol] = 1;
	
	//au-dessus
	if(idxLig+1<LIG && (*game).plateau[idxLig+1][idxCol]==initialChar){
		affichage_groupe(game, idxLig+1, idxCol);
	}
	//à droite
	if(idxCol+1<COL && (*game).plateau[idxLig][idxCol+1]==initialChar){
		affichage_groupe(game, idxLig, idxCol+1);
	}
	//en-dessous
	if(idxLig-1>=0 && (*game).plateau[idxLig-1][idxCol]==initialChar){
		affichage_groupe(game, idxLig-1, idxCol);
	}
	//à gauche
	if(idxCol-1>=0 && (*game).plateau[idxLig][idxCol-1]==initialChar){
		affichage_groupe(game, idxLig, idxCol-1);
	}
}

/*
int calcul_points(Game* game){
	int nbX = 0;
	for(int lig = 0; lig<LIG; lig++){
		for(int col = 0; col<(*game).nbColRestant; col++){
			if((*game).plateau[lig][col]=='x'){
				nbX++;
				(*game).plateau[lig][col] = '.';
			}
		}
	}
	return (nbX-1)*(nbX-1);
}
*/
