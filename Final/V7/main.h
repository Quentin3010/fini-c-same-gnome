#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define COL 15
#define LIG 10

#define reset "\033[0m"
#define reverse "\033[7m"
#define rouge "\033[;31m"
#define vert "\033[;32m"
#define bleu "\033[;34m"
#define jaune "\033[;33m"

typedef struct{
	char plateau[LIG][COL]; /*plateau du jeu de 15x10 cases*/
	int hasX[COL]; /*vecteur où chaque case correspond à une colonne du plateau. si has[X] = 1 alors il y a au moins un "x" dans plateau[0-9][X]*/
	int nbColRestant; /*nombre de colonne qui ont encore au moins une bille*/
	int nbTour; /*nombre de tour*/
	int score; /*score total du joueur*/
	int scoreTour; /*points qu'à marqué le joueur au tour d'avant*/
}Game; 

