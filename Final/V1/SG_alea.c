#include "main.h"

#include "game.h"
#include "saisie.h"
#include "update_plateau.h"
#include "gravite.h"

/********************************************************/
/* Action generate_plateau_random                       */
/* Role : Permet le remplissage du plateau              */
/*    Description des paramètres                        */
/*    game : Structure Game - Résultat                  */
/********************************************************/
/* Declaration et traitement */
void generate_plateau_random(Game* game){
	
	int i, j; /* indices de boucles */
	int a; /* variable qui récupère le random */
	
	/* Initialisation avec le temps du système */
	srand(time(NULL));
	
	for(i=0; i<LIG; i++){
		for(j=0; j<COL; j++){
		
			/* pour obtenir un entier aléatoire entre 1 et 3 */
			a = 1 + rand()%3;
			
			/* obtenir la valeur 1 avec la fonction random désignera que la bille est rouge */
			if(a==1){
				(*game).plateau[i][j] = 'R';
			}
			/* obtenir la valeur 2 avec la fonction random désignera que la bille est verte */
			else if(a==2){
				(*game).plateau[i][j] = 'V';
			}
			/* obtenir la valeur 3 avec la fonction random désignera que la bille est bleue */
			else{
				(*game).plateau[i][j] = 'B';
			}
		}
	}
}	

int main () {
	system("clear"); //vide le terminal

	Game game; /*Structure game correspondant à la partie en cours*/
	create_game(&game);
	
	generate_plateau_random(&game);
	
	int x,y; /*coordonnés de la ligne et de la colonne que le joueur à choisis*/	
	
	//Si le plateau est vide (case [0][0] vide), alors le jeu est fini
	while(verif_non_vide(game)!=0){
		//Le joueur choisit une case avec une bille
		x = -1; y = -1;
		saisie(game, &x, &y);
		
		//Le programme affiche toutes les billes voisines (change en "x")
		system("clear");
		affichage_groupe(&game, x, y);
		affichage_plateau(game);
		sleep(1);
		
		//On fait tomber les billes vers le bas (et on calcule le score)
		system("clear");
		gravite(&game);
		affichage_plateau(game);
		sleep(1);
		
		//On les regroupe vers la gauche
		system("clear");
		gravite_gauche(&game);
		
		//Et on passe au round suivant
		game.nbTour++;
	}
	
	system("clear");
	affichage_plateau(game);
	
	//Ecran de fin (score + nombre de tour) 
	sleep(2);
	system("clear");
	printf("*************************************************\n*                                               *\n*                                               *\n*                FELICITATION !!!               *\n*                                               *\n*                                               *\n*                 score : %5d                 *\n*                                               *\n*                  tour : %3d                   *\n*                                               *\n*                                               *\n*                                               *\n*************************************************\n", game.score, game.nbTour);
	return 0;
}
