# Same Gnome

Quentin BERNARD & Mathieu PALLATIN

## Description du projet

Le projet "Same Gnome", réalisé en langage C dans le cadre de notre scolarité en binôme. Same Gnome est un jeu où le principe est de détruire des cases qui sont connectées entre elles. Le joueur accumule des points en détruisant un nombre important de cases.

Dans Same Gnome, l'objectif est de rechercher et de sélectionner des groupes de cases identiques qui sont adjacentes les unes aux autres. 

Lorsque ces cases sont sélectionnées, elles sont détruites et le joueur marque des points. Plus le joueur parvient à détruire un grand nombre de cases en une seule fois, plus il récolte de points..

Vidéo de présentation : lien

## Fonctionnalités
.
Initialisation du plateau de jeu
Remplissage du plateau à partir d'un fichier txt et affichage de celui-ci.
Initialisation du plateau pour les fonctions "random" et "init math.c".
Algorithme de recherche des billes voisines
Gestion de l'écroulement des billes vers le bas
Décalage des colonnes vers la gauche
Changement des caractères en cases de couleurs
Ecran de fin de jeu
Score cumulé et du score au tour.

## Mon rôle

Dans le projet Same Gnome, j'ai joué un rôle central dans le développement des fonctionnalités essentielles. J'ai pris en charge l'initialisation et le remplissage du plateau de jeu à partir d'un fichier txt, ainsi que l'affichage correspondant. J'ai également mis en place l'algorithme de recherche des billes voisines, permettant aux joueurs de détruire les groupes de billes connectées. J'ai contribué à la fluidité du jeu en gérant l'écroulement des billes vers le bas après leur destruction. J'ai amélioré l'esthétique du jeu en convertissant les caractères en cases de couleurs. Enfin, j'ai ajouté des fonctionnalités telles qu'un écran de fin de jeu et l'affichage du score cumulé et du score au tour.
